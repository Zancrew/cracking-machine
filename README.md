# Cracking Machine

This project consists of a shell script using hashcat to automate the decryption of a password.
Please note that this tool is for **educational** purposes only.

## Installation

You can clone this repository by typing the following command:

```bash
git clone https://gitlab.com/Zancrew/cracking-machine.git
```

*Please ensure that git is installed.*


## Usage

Once cloned, go to this repository:

```bash
cd crack-shelter/
```

Then put the file containing your encrypted passwords there:

```bash
crack-shelter/YourFile.txt
```

Finally, run the shell script:

```bash
./cracking-machine
```

You may have permission problems. If this is the case, give the script the execution rights :

```bash
chmod +rwx ./cracking-machine
```

## Script 

The main actions to be performed by the user are described in the script. Here are some specifications to know before running it:  
1. This tool will first use **rockyou wordlist**, as the "first filter".
2. Then it will use the list of the top 10 cracked NTLM password masks of 2015. The masks are sorted in ascending order of decryption time.
3. Note that if the passwords you want to decrypt contain symbols, this tool will be **not adequate**. Password masks do not contain **charset** symbols.

## Sources
[Top 10 list of password masks](https://www.netspi.com/blog/technical/network-penetration-testing/netspis-top-password-masks-for-2015/#:~:text=cracked%20NTLM%20passwords.-,The%20Top%2010,-%23)  
[Hashcat github repository](https://github.com/hashcat/hashcat)

