#!/bin/bash

hash="\nHash not found. Script won't work.\n"

# Display the instructions.
printf "Welcome to this password decryption script."
sleep 1

# Get the user input (hash file name, hash type)
printf "\n\nSpecify the file name, with extension (YourFile.txt) :\nFile name : "
read file_name

# Get the hash type
printf "\n\nSupported hash types : NTLM / MD5 / MD4 / SHA1 / SHA2-256 / SHA3-256"
printf "Specify the hash type (see supported types above) :\nHash type : "
read hash_type

# Get the wanted speed
printf "Specify the workload :\n1. Slow\n2. Normal\n3. Fast\n4. Ultra fast\nWorkload : "
read workload

if [[ $workload == 0 || $workload > 4 ]]; then
  printf "\nError : invalid number.\nPlease refer to the valid numbers above.\n"
  exit 1 
fi

case $hash_type in

  NTLM)
  hash=1000
  ;;

  MD5)
  hash=0
  ;;

  MD4)
  hash=900
  ;;

  SHA1)
  hash=100
  ;;

  SHA2-256)
  hash=1400
  ;;

  SHA3-256)
  hash=17400
  ;;
esac

echo "\n\n\nHash type : $hash_type ($hash)"

# Create a function which changes the mask.
my_function () {
  printf "\n\nGoing through the n°$1 mask.\n"
  hashcat -m $2 -a 3 $3 $4 -O -w $workload
  hashcat -m $2 $3 --show
}

# Go through the rockyou wordlist.
printf "\n\nGoing through rockyou.txt wordlist."
hashcat -m $hash -a 0 $file_name rockyou.txt -O -w $workload
hashcat -m $hash $file_name --show


if [[ $hash != "\nHash not found. Script won't work.\n" ]]; then
  # Example password : Fall2015 | 2,91% of Matching Cracked Passwords | 40s at 100MH/s.
  my_function "1" "$hash" "$file_name" "?u?l?l?l?d?d?d?d"

  # Example password : March123 | 1,98% of Matching Cracked Passwords | 2m at 100MH/s.
  my_function "2" "$hash" "$file_name" "?u?l?l?l?l?d?d?d"

  # Example password : Spring15 | 5,45% of Matching Cracked Passwords | 5m at 100MH/s.
  my_function "3" "$hash" "$file_name" "?u?l?l?l?l?l?d?d"

  # Example password : January1 | 2,34% of Matching Cracked Passwords | 12m at 100MH/s.
  my_function "4" "$hash" "$file_name" "?u?l?l?l?l?l?l?d"

  # Example password : March2015 | 2,79% of Matching Cracked Passwords | 18m at 100MH/s.
  my_function "5" "$hash" "$file_name" "?u?l?l?l?l?d?d?d?d"

  # Example password : August123 | 1,51% of Matching Cracked Passwords | 50m at 100MH/s.
  my_function "6" "$hash" "$file_name" "?u?l?l?l?l?l?d?d?d"

  # Example password : January15 | 4,14% of Matching Cracked Passwords | 3h at 100MH/s.
  my_function "7" "$hash" "$file_name" "?u?l?l?l?l?l?l?d?d"

  # Example password : February1 | 1,72% of Matching Cracked Passwords | 5h30 at 100MH/s.
  my_function "8" "$hash" "$file_name" "?u?l?l?l?l?l?l?l?d"

  # Example password : Winter2015 | 2,72% of Matching Cracked Passwords | 16h at 100MH/s.
  my_function "9" "$hash" "$file_name" "?u?l?l?l?l?l?d?d?d?d"

  # Example password : December15 | 3,10% of Matching Cracked Passwords | 2d6h at 100MH/s.
  my_function "10" "$hash" "$file_name" "?u?l?l?l?l?l?l?l?d?d"

else
  "Hash type not found. Please specify one."
fi